## where to get
http://lftp.yar.ru/ftp/

## how to build
```bash
CC="afl-clang-fast" CXX="afl-clang-fast++" CFLAGS="-O0 -g -fsanitize=address" CXXFLAGS="-O0 -g -fsanitize=address" LDFLAGS="-ldl -fsanitize=address" ./configure --without-gnutls --with-openssl
```

## how to run afl-fuzz
```bash
afl-fuzz -i ${input} -o ${output} ${path_to_lftp} -u demo,password ftp://test.rebex.net/
```
